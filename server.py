from bottle import (
    route, run, template, get, post, static_file
)
import os
import json
import sane
import threading

config = json.loads(open('config.json', 'r').read())
desired_scanner_id = config.get("desired_scanner_id")

index_tmpl = open('templates/index.html', 'r').read()


def start_the_actual_scan_job(job: 'Job') -> None:

    ver = sane.init()
    print('sane: version', ver)
    job.state(Job.SaneInitiated)

    devices = sane.get_devices()
    print('sane: devices', devices)
    job.state(Job.SaneDiscoveredDevices)

    desired_scanner_id_found = False

    for device in devices:
        device_id = device[0]
        if device_id in desired_scanner_id:
            desired_scanner_id_found = True

    if not desired_scanner_id_found:
        job.finish(True, 'Desired scanner not found')
        return
    
    print('sane: desired scanner found')
    job.state(Job.SaneDesiredDeviceFound)

    try:
        dev = sane.open(desired_scanner_id)
    except Exception as e:
        print('sane: exception while opening device', e)
        job.finish(True, 'Can not open desired scanner device')
        return

    job.state(Job.SaneDesiredDeviceReady)

    parameters = dev.get_parameters()
    print('sane: parameters', parameters)

    try:
        dev.start()
        job.state(Job.SaneScanJustInitiated)
        im = dev.snap()
        im.save('/tmp/insane/insane-preview.png')
        job.state(Job.SanePreviewHasGenerated)
    except Exception as e:
        print('sane: exception wile trying to scan image', e)
        job.finish(True, 'Can not do scan via desired scanner')
        return

    dev.close()
    print('sane: device has been closed')
    job.state(Job.SaneDesiredDeviceClosed)

    job.finish(False, '')


class Job:

    # States
    SaneInitiated = 'SaneInitiated'
    SaneDiscoveredDevices = 'SaneDiscoveredDevices'
    SaneDesiredDeviceFound = 'SaneDesiredDeviceFound'
    SaneDesiredDeviceReady = 'SaneDesiredDeviceReady'
    SaneScanJustInitiated = 'SaneScanJustInitiated'
    SanePreviewHasGenerated = 'SanePreviewHasGenerated'
    SaneDesiredDeviceClosed = 'SaneDesiredDeviceClosed'

    # other fields

    _state: str = ''
    _error: bool = False
    _explanation: str = ""
    _finished: bool = False

    def state(self, state: str) -> None:

        self._state = state

    def start(self) -> None:

        t = threading.Thread(
                target=start_the_actual_scan_job, args=(self,))
        t.start()

    def finish(self, error: bool, explanation: str) -> None:

        self._error = error
        self._explanation = explanation
        self._finished = True

    def finished(self) -> bool:

        return self._finished

    def status(self) -> dict:

        return {
            "finished": self._finished,
            "state": self._state,
            "error": self._error, "why": self._explanation
        }


class InsaneApp:

    _job:  Job = None

    config: dict = {"host": "0.0.0.0", "port": 8000}

    def __init__(self) -> None:

        if not os.path.exists('/tmp/insane'):
            os.mkdir('/tmp/insane')

    def get_job_status(self) -> dict:

        return self._job.status()

    def start_a_new_scan_job(self) -> None:

        self._job = Job()
        self._job.start()

    def is_currently_busy(self) -> bool:

        return self._job and not self._job.finished()

    def is_currently_busy_js(self) -> str:

        return "true" if self.is_currently_busy() else "false"


app = InsaneApp()


@route('/')
def index():
    return template(index_tmpl,
                    scaner_is_busy=app.is_currently_busy_js())


@post('/scan')
def init_scan_job():
    if app.is_currently_busy():
        return {"declined": True}
    else:
        app.start_a_new_scan_job()
        return {"declined": False}
    

@get('/scan')
def check_scan_job():
    return app.get_job_status()


@route('/preview.png')
def preview_png():
    return static_file('insane-preview.png', root='/tmp/insane')


run(**app.config)  # run a Bottle.py web-server using app config
